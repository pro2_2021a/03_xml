package experiments;

import org.w3c.dom.*;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.UnknownHostException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Scanner;

public class AvailabilityReport
{
    public static void main(String[] args) throws IOException, ParserConfigurationException, TransformerException {
        Scanner scanner = new Scanner(System.in);
        scanner.useDelimiter("[ ,;\n]");
        System.out.println("Prosím, zadejte maximální počet pokusů o připojení:");
        int atttemptsCount = scanner.nextInt();
        System.out.println("Prosím, zadejte webové adresy:");
        ArrayList<String> urls = new ArrayList();

        while(true)
        {
            String url = scanner.next();
            urls.add(url);
            if(url.equals("END"))
            {
                break;
            }
        }

        DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder documentBuilder = documentBuilderFactory.newDocumentBuilder();

        Document document = documentBuilder.newDocument();
        Element htmlElement = document.createElement("html");
        htmlElement.setAttribute("xmlns", "http://www.w3.org/1999/xhtml");
        document.appendChild(htmlElement);
        Element bodyElement = document.createElement("body");
        htmlElement.appendChild(bodyElement);
        Element ulElement = document.createElement("ul");
        bodyElement.appendChild(ulElement);

        for (String url1 : urls)
        {
            boolean ok;
            try
            {
                int response = Utils.GetResponseCodeFromURL(url1);
                ok = true;
            }
            catch (MalformedURLException malformedURLException)
            {
                ok = false;
            }
            catch (UnknownHostException exception)
            {
                ok = false;
            }

            Element liElement = document.createElement("li");
            ulElement.appendChild(liElement);
            Element spanElement = document.createElement("span");
            liElement.appendChild(spanElement);

            if(ok)
            {
               spanElement.setTextContent("Stranka "+url1+" byla dostupna");
            }
            else
            {
                spanElement.setTextContent("stranka "+url1+ " nebyla dostupna");
            }
        }

        Path path = Paths.get(System.getProperty("user.dir"), "resport.xhtml");
        Utils.writeXml(document, new FileOutputStream(path.toString()));



        System.out.println("Hotovo. Report najdete v následujícím souboru:");
    }
}
